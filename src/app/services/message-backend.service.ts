import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AddMessageMessage} from '../classes/add-message-message';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators';
import {GetMessagesResponse} from '../classes/get-messages-response';
import {environment} from '../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable()
export class MessageBackendService {

    private messageUrl = environment.apiHost + '/api/v1/messages/';

    constructor(private http: HttpClient) {
    }

    public addMessage(newMessage: AddMessageMessage): Observable<{}> {
        return this.http.post<AddMessageMessage>(this.messageUrl, newMessage, httpOptions)
            .pipe(
                catchError(this.handleError<AddMessageMessage>('addMessage'))
            );
    }

    public getMessages(onlyPersonal: boolean): Observable<GetMessagesResponse> {
        const url = this.messageUrl + (onlyPersonal ? '?view=own' : '');
        return this.http.get<GetMessagesResponse>(url, httpOptions)
            .pipe(
                catchError(this.handleError<GetMessagesResponse>('getMessage'))
            );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    private log(message: string) {
        console.log('MessageBackendService: ' + message);
    }
}
