import { TestBed, inject } from '@angular/core/testing';

import { MessageBackendService } from './message-backend.service';

describe('MessageBackendService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MessageBackendService]
    });
  });

  it('should be created', inject([MessageBackendService], (service: MessageBackendService) => {
    expect(service).toBeTruthy();
  }));
});
