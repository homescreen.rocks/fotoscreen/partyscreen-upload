import {Component, OnInit} from '@angular/core';
import {MessageBackendService} from '../../services/message-backend.service';
import {MessageItem} from '../../classes/message-item';
import {GetMessagesResponse} from '../../classes/get-messages-response';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
    messages: MessageItem[];
    pictureHost = '';
    env = '';

    constructor(private messageBackendService: MessageBackendService) {
        this.env = environment.apiHost;
    }

    ngOnInit() {
        this.reloadMessages();
        this.pictureHost = environment.apiHost;
    }

    reloadMessages() {
        this.messageBackendService.getMessages(false).subscribe((response: GetMessagesResponse) => {
            this.messages = response.items;
        });
    }
}
