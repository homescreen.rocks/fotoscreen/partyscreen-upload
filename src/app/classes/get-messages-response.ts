import {MessageItem} from './message-item';

export class GetMessagesResponse {
    public items: MessageItem[];
}
